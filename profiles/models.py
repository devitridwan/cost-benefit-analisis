# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
from datetime import date

date_format = date.today().year - 25

class Profile(models.Model):
    kd_profile = models.CharField(max_length=10, primary_key=True, verbose_name='Kode Profile')
    nm_profile = models.CharField(max_length=30, verbose_name='Nama')
    user_active = models.BooleanField(default=False)
    tgl_lahir = models.DateField(default=date(date_format,1,1), verbose_name='Tanggal Lahir')
    alamat_skr = models.CharField(max_length=75, verbose_name='Alamat Sekarang')
    email_profile = models.EmailField(blank=True)
    
    def __unicode__(self):
        return self.nm_profile
    
    class Meta:
        ordering = ['kd_profile']

class UserProfileManager(BaseUserManager):
    def create_user(self, email, password=None):
        """
        User harus mempunyai email
        """
        if not email:
            raise ValueError('User diwajibkan memiliki email')
            user = self.model(
                    email = self.normalize_email(email),
                    )
            user.set_password(password)
            user.save(using=self._db)
            return user
    def create_superuser(self, email, password):
        user = self.create_user(
                email,
		password=password
		)
	user.is_admin = True
	user.save(using=self._db)
	return user

class UserProfile(AbstractBaseUser):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, blank=True, null=True, unique=True)
    email = models.EmailField(verbose_name='email', max_length=255,unique=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    objects = UserProfileManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELD = []

    def get_full_name(self):
        return self.profile.nm_profile

    def get_short_name(self):
	pass

    def __unicode__(self):
	return self.email

    def has_perm(self, perm, obj=None):
	return True

    def has_module_perms(self, app_label):
	return True

    @property
    def is_staff(self):
	return self.is_active
