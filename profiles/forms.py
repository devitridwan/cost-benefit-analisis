from django import forms
from django.contrib.auth.models import User
from suit.widgets import LinkedSelect
from .models import Profile, UserProfile
from django.contrib.auth.forms import ReadOnlyPasswordHashField

class UserCreationForm(forms.ModelForm):
    #nm_profile = forms.ModelChoiceField(Profile.objects.filter(username__isnull=True)
    #password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    #password2 = forms.CharField(label='Password Confirmation', widget=forms.PasswordInput)

    class Meta:
        model = UserProfile
        fields = ('profile', 'email')

    """def clean_password2(self):
        #password1 = self.cleaned_data.get("password1")
        #password2 = self.cleaned_data.get("password2")
        #if password1 and password2 and password1 != password2:
        #    raise forms.ValidationError("Password tidak sama")
        #return password2
        get_profile = self.cleaned_data.get("profile")
        password1 = """

    def save(self, commit=True):
        profile = self.cleaned_data.get("profile")
        depan = ""
        n = 0
        while n < len(profile.nm_profile):
            if profile.nm_profile[n].isupper():
                depan = depan + str(profile.nm_profile[n])
            n = n + 1
        hari = str(profile.tgl_lahir.day)
        if len(hari)==1: hari = "0"+hari
        bulan = str(profile.tgl_lahir.month)
        if len(bulan)==1: bulan = "0"+bulan

        password = depan+hari+bulan+str(profile.tgl_lahir.year)[-2:]
        #password = "haiakusukakamu"
        user = super(UserCreationForm, self).save(commit=False)
        #user.set_password(self.cleaned_data["password"])
        user.set_password(password)
        p = Profile.object.get(pk=profile.pk)
        p.user_active = True
        if commit:
            user.save()
            p.save
        return user

class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField
    class Meta:
        model = UserProfile
        fields = ('profile','email', 'is_admin')

    def clean_password(self):
        return self.initial["password"]

class ProfileChangeForm(forms.ModelForm):
    if Profile.objects.count()<=0:
        kd_profile = forms.CharField(max_length=10, initial='100000000', widget=forms.TextInput(attrs={'readonly':'readonly'}))
    else:
        get_kode = int(Profile.objects.last().pk)+1
        kd_profile = forms.CharField(max_length=10, initial=str(get_kode), widget=forms.TextInput(attrs={'readonly':'readonly'}))
    class Meta:
        model = Profile
        fields = '__all__'
