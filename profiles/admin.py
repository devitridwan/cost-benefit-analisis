# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .forms import UserChangeForm, UserCreationForm, ProfileChangeForm
from .models import Profile, UserProfile

admin.site.unregister(Group)

class UserAdmin(BaseUserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm

    list_display = ('email', 'profile', 'is_admin')
    list_filter = ('is_admin',)
    fieldsets = (
            (None,{'fields':('profile', 'email')}),
            ('Permission',{'fields':('is_admin',)}),
        )

    add_fieldsets = (
            (None,{
                    'classes':('wide',),
                    'fields':('profile','email')
                }),
        )

    search_fields = ('profile', 'email')
    ordering = ('profile','email')
    filter_horizontal = ()

    def get_queryset(self, request):
        qs = super(UserAdmin, self).get_queryset(request)
        if not request.user.is_admin:
            return qs.filter(email=request.user)
        else:
            return qs

    def has_delete_permission(self, request, obj=None):
        if not request.user.is_admin or request.user==obj:
            return False
        return True

    def has_add_permission(self, request):
        if not request.user.is_admin:
            return False
        return True

    def get_form(self, request, *args, **kwargs):
        form = super(UserAdmin, self).get_form(request, *args, **kwargs)
        form.base_fields['profile'].queryset = Profile.objects.filter(user_active=False)
        if not request.user.is_admin:
            form.base_fields['is_admin'].widget.attrs["disabled"] = "disabled"
            form.base_fields['profile'].widget.attrs["disabled"] = "disabled"
        return form

class ProfileAdmin(admin.ModelAdmin):
    form = ProfileChangeForm

    list_display = ('kd_profile', 'nm_profile', 'user_active', 'email_profile')
    list_filter = ('user_active',)
    search_fields = ('kd_profile', 'nm_profile', 'email_profile')
    ordering = ('kd_profile', 'nm_profile', 'email_profile')

    fieldsets = (
            (None,{'fields':('kd_profile', 'nm_profile','tgl_lahir','alamat_skr')}),
            ('Permission',{'fields':('user_active',)}),
            ('Optional',{'fields':('email_profile',)})
        )

    def get_queryset(self, request):
        qs = super(ProfileAdmin, self).get_queryset(request)
        if not request.user.is_admin:
            return qs.filter(kd_profile=request.user.profile.pk)
        else:
            return qs

    def has_delete_permission(self, request, obj=None):
        if not request.user.is_admin or request.user.profile==obj:
            return False
        return True

    def has_add_permission(self, request):
        if not request.user.is_admin:
            return False
        return True

    def get_form(self, request, *args, **kwargs):
        form = super(ProfileAdmin, self).get_form(*args, **kwargs)
        form.base_fields['user_active'].widget.attrs["disabled"] = "disabled"
        return form

admin.site.register(UserProfile, UserAdmin)
admin.site.register(Profile, ProfileAdmin)
