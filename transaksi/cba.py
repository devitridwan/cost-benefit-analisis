from .models import Container, BebanBulan, Modal, Pendapatan, SukuBunga

beban = BebanBulan.objects.all()
modal = Modal.objects.all()
pendapatan = Pendapatan.objects.all()
container = Container.objects.all()
if SukuBunga.objects.count()<=0:
    bunga=0
else:
    bunga = SukuBunga.objects.all()[0].suku_bunga

def tot_pendapatan():
    total = 0
    list_pendapatan = []
    for con in container:
        pendapatan = Pendapatan.objects.filter(kd_pendapatan=con.pk)
        beban = BebanBulan.objects.filter(kd_beban=con.pk)
        tot_pendapatan = 0
        tot_beban = 0
        for pen in pendapatan:
            tot_pendapatan = tot_pendapatan + pen.nominal_pendapatan
        for beb in beban:
            tot_beban = tot_beban + beb.nominal_beban
        total = total + (tot_pendapatan-tot_beban)
    return total

def tot_beban():
    total = 0
    for tot in beban:
        total = total + tot.nominal_beban
    return total

def tot_modal():
    total = 0
    for tot in modal:
        total = total + tot.nominal_modal
    return total

#def tot_pendapatan():
#    total = 0
#    for tot in pendapatan:
#        total = total +tot.nominal_pendapatan
#    return total

#from decimal import Decimal, getcontext
def cba_tool():
    roi = "0"
    npv = "0"
    pp = "0"
    bcr = "0"
    if not (tot_beban()==0 or tot_modal()==0 or tot_pendapatan()==0):
        from decimal import Decimal, getcontext
        getcontext().prec=9
        dec = Decimal
	roi = (dec(tot_pendapatan())-dec(tot_modal()))/dec(tot_modal())
        #npv = (tot_modal()+tot_beban())+tot_pendapatan()/(1+SukuBunga.suku_bunga)**1
        npv = dec(-tot_modal())+(dec(tot_pendapatan())/dec(1)+bunga)
        tot_pp = (dec(tot_modal())/dec(tot_pendapatan()))*dec(12)
        tahun = tot_pp/12
        bulan = tot_pp%12
        pp = "%d Tahun %d Bulan" % (tahun, bulan)
        #sementara
        bcr = (dec(tot_pendapatan())/dec(1)+dec(bunga))/(dec(tot_modal())/dec(1)+dec(bunga))
        #bcr = (tot_pendapatan()/1)/(tot_beban()/1)
        persen = ((roi+tot_pp+bcr)/3)*100

    return {'roi':roi, 'npv':npv, 'pp':pp, 'bcr':bcr, 'persen':persen}
