# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from datetime import date
from decimal import Decimal

TAHUN = ()
thn = date.today().year - 100
while thn < date.today().year:
    TAHUN = TAHUN + (((str(thn+1),str(thn+1))),)
    thn += 1

BULAN = (
    ('1', 'Januari'),
    ('2', 'Februari'),
    ('3', 'Maret'),
    ('4', 'April'),
    ('5', 'Mei'),
    ('6', 'Juni'),
    ('7', 'Juli'),
    ('8', 'Agustus'),
    ('9', 'September'),
    ('10', 'Oktober'),
    ('11', 'November'),
    ('12', 'Desember')
    )
bulan = date.today().month

class Container(models.Model):
    created = models.DateField(default=date.today())
    kd_container = models.CharField(max_length=7, primary_key=True,
                                    verbose_name='Kode Transaksi')
    bulan = models.CharField(max_length=2, choices=BULAN, default=str(bulan))
    tahun = models.CharField(max_length=4,choices=TAHUN,default=str(date.today().year))
    nama_container = models.CharField(max_length=25, verbose_name = 'Nama Transaksi')


    def __unicode__(self):
        return self.kd_container

    class Meta:
        verbose_name_plural = 'Cost Benefit Analysis'
        verbose_name = 'Cost Benefit Analysis'
        ordering = ['kd_container']

class Modal(models.Model):
    kd_modal = models.ForeignKey(Container, verbose_name='Kode Account')
    modal_update = models.DateField(default=date.today(), verbose_name='Last Update')
    #modal_bulan = models.CharField(max_length=2, choices=BULAN, default=str(bulan))
    nm_modal = models.CharField(max_length=100, verbose_name='Nama Account')
    jml_item = models.PositiveIntegerField(default=1, verbose_name='Jumlah Item')
    nominal_modal = models.PositiveIntegerField(verbose_name='Nominal')

    def __unicode__(self):
    	return self.nm_modal

    class Meta:
        verbose_name_plural = 'Accounts'

class BebanBulan(models.Model):
    kd_beban = models.ForeignKey(Container, verbose_name='Kode Pengeluaran')
    beban_update = models.DateField(default=date.today(), verbose_name='Last Update')
    #beban_bulan = models.CharField(max_length=2, choices=BULAN, default=str(bulan))
    nm_beban = models.CharField(max_length=100, verbose_name='Nama Pengeluaran')
    jml_item_beban = models.PositiveIntegerField(default=1, verbose_name='Jumlah Item')
    nominal_beban = models.PositiveIntegerField(verbose_name='Nominal')

    def __unicode__(self):
        return self.nm_beban
    class Meta:
        verbose_name_plural='Beban per Bulan'

class Pendapatan(models.Model):
    kd_pendapatan = models.ForeignKey(Container, verbose_name='Kode Pendapatan')
    pendapatan_update = models.DateField(default=date.today(), verbose_name='Last Update')
    #pendapatan_bulan = models.CharField(max_length=2, choices=BULAN, default=str(bulan))
    pendapatan_tahun = models.CharField(max_length=4, choices=TAHUN, default=str(date.today().year))
    nm_pendapatan = models.CharField(max_length=25, verbose_name='Nama Pendapatan')
    nominal_pendapatan = models.PositiveIntegerField(verbose_name='Nominal')

    def __unicode__(self):
	return self.nm_pendapatan

    class Meta:
	verbose_name_plural = 'Pendapatan'

class CBA(models.Model):
	roi = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Return On Investment')
	npv = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Net Present Value')
	pp = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Payback Period')
	bcr = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Benefit/Cost Ratio')

	class Meta:
		verbose_name_plural='Benefit Cost Analisis'

class SukuBunga(models.Model):
    nama = models.CharField(max_length=10, default='Suku Bunga')
    suku_bunga = models.DecimalField(max_digits=3, decimal_places=2, default=Decimal(0.00))
    def __unicode__(self):
        return self.nama
    
    class Meta:
        verbose_name_plural = 'Suku Bunga'

class Sumary(Container):
	class Meta:
		proxy = True
		verbose_name='Cost Benefit Analisis'
		verbose_name_plural='Cost Benefit Analisis'
