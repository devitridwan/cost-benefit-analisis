#-*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from datetime import date
from django.contrib.auth.models import User
from .models import BebanBulan, Modal, Pendapatan, Container, SukuBunga
from .forms import ModalForm, BebanBulanForm, PendapatanForm, ContainerForm, SukuBungaForm
from cba import cba_tool

class ModalInline(admin.StackedInline):
    form = ModalForm
    model = Modal
    extra = 1
    verbose_name='Accounts'
    suit_classes = 'suit-tab suit-tab-accounts'

class BebanBulanInline(admin.StackedInline):
    form = BebanBulanForm
    model = BebanBulan
    extra = 1
    verbose_name='Pengeluaran'
    suit_classes = 'suit-tab suit-tab-bebanbulan'

class PendapatanInline(admin.StackedInline):
    form = PendapatanForm
    model = Pendapatan
    extra = 1
    verbose_name='Pendapatan'
    suit_classes = 'suit-tab suit-tab-pendapatan'

"""class ContainerAdmin(admin.ModelAdmin):
	form = ContainerForm
	inlines = (ModalInline, BebanBulanInline, PendapatanInline)

	suit_form_tabs = (('accounts', 'Accounts'), ('pendapatan', 'Pendapatan'), ('bebanbulan', 'Pengeluaran'))
	suit_form_includes =(
		('beban/beban-bulan-form.html','','bebanbulan'),
		)

admin.site.register(Container, ContainerAdmin)"""

@admin.register(Container)
class CbaAdmin(admin.ModelAdmin):
    change_list_template = 'admin/cba_admin.html'
    #change_form_template = 'admin/cba_admin_form.html'
    form = ContainerForm
    list_display = ('kd_container', 'nama_container', 'bulan','tahun' , 'created')
    inlines = (ModalInline, BebanBulanInline, PendapatanInline)
    suit_form_tabs = (('accounts', 'Accounts'), ('pendapatan', 'Pendapatan'), ('bebanbulan', 'Pengeluaran'))
    def changelist_view(self, request, extra_context=None):
       # response = super(CbaAdmin, self).changelist_view(request, extra_context)
	#qs = response.context_data['cl'].queryset
        extra_context = {
            'roi':cba_tool()['roi'],
            'npv':cba_tool()['npv'],
            'pp':cba_tool()['pp'],
            'bcr':cba_tool()['bcr'],
        }
        #response.context_data.update(extra_context)
        #return response
        return super(CbaAdmin, self).changelist_view(request, extra_context)

    def save_model(self, request, obj, form, change):
        if not change:
            if SukuBunga.objects.count()<=0:
                SukuBunga.objects.create(nama="Suku Bunga", suku_bunga=0)

@admin.register(SukuBunga)
class SukuBungaAdmin(admin.ModelAdmin):
    form = SukuBungaForm
    list_display = ('nama','suku_bunga')
    def save_model(self, request, obj, form, change):
        #obj = SukuBunga.objects.
        if not change:
            if SukuBunga.objects.count()>0:
                from django.contrib import messages
                messages.error(request, 'Suku Bunga tidak boleh ditambah')
