from .models import BebanBulan, Modal, Pendapatan, Container, SukuBunga
from django import forms
from suit.widgets import NumberInput, EnclosedInput, SuitDateWidget

class BebanBulanForm(forms.ModelForm):
    class Meta:
        model = BebanBulan
        fields = ('beban_update','nm_beban','nominal_beban')
        widgets = {
			'beban_update':forms.TextInput(attrs={'readonly':'readonly'}),
			'nm_beban':forms.TextInput(attrs={'class':'input-xlarge'}),
			'jml_item_beban':NumberInput(attrs={'class':'input-mini'}),
			'nominal_beban':EnclosedInput(prepend='Rp', append='.00')
		}

class ModalForm(forms.ModelForm):
    class Meta:
        model = Modal
        fields = '__all__'
        widgets = {
            'modal_update':forms.TextInput(attrs={'readonly':'readonly','class':'input-medium'}),
            'nm_modal':forms.TextInput(attrs={'class':'input-xlarge'}),
            'jml_item':NumberInput(attrs={'class':'input-mini'}),
            'nominal_modal':EnclosedInput(prepend='Rp', append='.00')
		}

class PendapatanForm(forms.ModelForm):
    class Meta:
        model = Pendapatan
        fields = '__all__'
        widgets = {
            'pendapatan_update':forms.TextInput(attrs={'readonly':'readonly'}),
            'tgl_pendapatan':forms.DateField(widget=forms.SelectDateWidget),
            'nm_pendapatan':forms.TextInput(attrs={'class':'input-xlarge'}),
            'nominal_pendapatan':EnclosedInput(prepend='Rp', append='.00')
		}

class ContainerForm(forms.ModelForm):
    if Container.objects.count()<=0:
        kd_container = forms.CharField(max_length=10, initial='101', widget=forms.TextInput(attrs={'readonly':'readonly'}))
    else:
        get_kode = int(Container.objects.last().pk)+1
        kd_container = forms.CharField(max_length=10, initial=str(get_kode), widget=forms.TextInput(attrs={'readonly':'readonly'}))
    class Meta:
        model = Container
        fields = '__all__'
    
    #def clean(self):
    #    kode = self.cleaned_data.get('kd_container', None)
    #    beban = BebanBulan.objects.filter(kd_beban = kode)
    #    pendapatan = Pendapatan.objects.filter(kd_pendapatan = kode)
    #    modal = Modal.objects.filter(kd_modal = kode)
    #    if beban.count()==0 and pendapatan.count()==0 and modal.count()==0:
    #        #from django.contrib import messages
    #        #messages.error(request, 'Error:Null')
    #        from django.core.exceptions import ValidationError
    #        raise ValidationError('Error:Null')
    #    
    #    if SukuBunga.objects.all().count()==0:
    #        SukuBunga.objects.create(suku_bunga=0.00)
#        return kode
    #def __init__(self, *args, **kwargs):

    #def clean(self):
    #    cleaned_data = super(ContainerForm, self).clean()
    #    kode = cleaned_data("kd_container")
    #    beban = BebanBulan.objects.filter(kd_beban=kode)

class SukuBungaForm(forms.ModelForm):
    nama = forms.CharField(max_length=10, initial='Suku Bunga', widget=forms.TextInput(attrs={'readonly':'readonly'}))
    class Meta:
        model = SukuBunga
        fields = '__all__'
